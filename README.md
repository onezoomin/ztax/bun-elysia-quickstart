# zod tRPC via Elysia on Bun
Main idea here is to create an endpoint with basic CRUD functionality,  
by simply designing a zod schema.

## Getting Started
To get started with this template, 

install bun following instructions here:
https://github.com/oven-sh/bun#readme

and the elysia tRPC libs:
```bash
bun add @elysiajs/trpc @trpc/server @elysiajs/websocket 
```

## Development
To start the development server run:
```bash
bun run dev
```


Hit some end points, eg:
 - http://localhost:8888/trpc/greet?input=%22foo%22
 - http://localhost:8888/trpc/updateuser?input={%22uuid%22:%2233qq33%22}