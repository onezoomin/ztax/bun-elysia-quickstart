import type Elysia from 'elysia'
import type { Static, TObject, TProperties } from '@sinclair/typebox'

// import { t } from 'elysia'
import type { objectsToCrud } from 'index'

export const addCreateRoute = (zodObject: Static<TObject>, e: Elysia) => {
  return e
}

export const addReadRoute = (zodObject: Static<TObject>, e: Elysia) => {
  return e
}

export const addUpdateRoute = (zodObject: Static<TObject>, e: Elysia) => {
  return e
}

export const addDeleteRoute = <T extends TObject>(path: string, query: T, e: Elysia) => {
  // const { properties: query } = tbObj
  return e.get(
    `/del${path}/:uuid`,
    ({ query: { uuid: uuidQuery, ...restParams }, params: { uuid } }) => `del:${uuid || uuidQuery}${JSON.stringify(restParams)}`,
    { schema: { query } },
  )
}

export const autoCRUDrouter = (objsToCrud: typeof objectsToCrud/* Record<string, TObject> */, e: Elysia) => {
  e.get('/ping', () => 'pong')

  for (const [eachObjectKey, eachTypeBoxObject] of Object.entries(objsToCrud)) {
    console.log('key --', eachObjectKey)
    console.log('tb --', eachTypeBoxObject)
    // addCreateRoute(eachObjectKey, eachTypeBoxObject, e)
    // addReadRoute(eachObjectKey, eachTypeBoxObject, e)
    // addUpdateRoute(eachObjectKey, eachTypeBoxObject, e)
    addDeleteRoute(eachObjectKey, eachTypeBoxObject, e)
  }
  // console.log({ e })
  return e
}
