import { autoCRUDrouter } from 'autorouter'
import { Elysia } from 'elysia'
import { swagger } from '@elysiajs/swagger'
// import type { Static, TObject } from '@sinclair/typebox'
import { Type as t } from '@sinclair/typebox'
export const objectsToCrud = {
  user: t.Object({
    uuid: t.Optional(t.String()),
    name: t.Optional(t.String()),
    birthYear: t.Optional(t.Integer()),
    userint: t.Optional(t.Integer()),
  }),
  keys: t.Object({
    user: t.String(), // user.uuid
    publicSigningKey: t.String(),
    did: t.String(),
    keysint: t.Integer(),
  }),
} as const // Record<string, Static<TObject>>

const app = new Elysia()
export type ElysiaApp = typeof app
autoCRUDrouter(objectsToCrud, app)

app
  .use(swagger())
  .listen(8888)

console.log(app)
